import pandas as pd
import os
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine


uncategorized_usage_stats = """
WITH live_data AS (

	SELECT DISTINCT full_ping_name
	FROM analytics_staging.version_usage_stats_list

), category_mapping AS (

	SELECT DISTINCT stats_used_key_name
	FROM analytics_staging.version_usage_stats_to_stage_mappings

)

SELECT
  *
FROM live_data
FULL OUTER JOIN category_mapping
  ON live_data.full_ping_name = category_mapping.stats_used_key_name
WHERE (category_mapping.stats_used_key_name IS NULL
  OR live_data.full_ping_name IS NULL)
"""


def main(query_name):
    snowflake_url = URL(
        account="gitlab",
        user=os.environ["SNOWFLAKE_TRANSFORM_USER"],
        password=os.environ["SNOWFLAKE_PASSWORD"],
        database="ANALYTICS",
        warehouse="TRANSFORMING_XS",
        role="TRANSFORMER",
    )
    # authenticator='https://gitlab.okta.com',
    engine = create_engine(snowflake_url)
    connection = engine.connect()
    df1 = pd.read_sql(sql=query_name, con=connection)
    print(df1)


## main
if __name__ == "__main__":
    ## start snowflake engine
    print("starting")
    ## execute query that produces failure result
    main(query_name=uncategorized_usage_stats)
    print("done")
    ## enhance result, e.g. zuora url
