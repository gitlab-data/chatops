# chatops

The purpose of this project is to make triaging easier by providing commands that can be run in Slack which will return the specific rows where the `dbt-test` errors happened. This is usually a first step that needs to be addressed when a [dbt test fails](https://gitlab.com/gitlab-data/analytics/-/tree/master/transform/snowflake-dbt/tests).

## How to use it

Go to slack and type `/gitlab datachat run "name of command"` where "name of command" is any of the ones defined in the `.gitlab-ci.yml` file.

For example, if the `dbt-test` `missing_location_factor` fails, the command to use would be:
`/gitlab datachat run missing_location_factor`.

## Adding a new chatops command

1. Create a new file with the name of the test you want to be able to run in Slack
1. Copy the contents of one of the current `python` or `.py` files in this project and paste it in the file created in the first step
1. Inside the newly created file rename the variable used for the string assignment and replace the query to be the one of the test you want to automate. For example, if in step 2 we choose to use the `bamboo_missing_location_factor.py` file as the template one, we would rename the variable `no_missing_location_factors` to an appropiate name for the test we want to run and change the query inside it
1. Go to `.gitlab-ci.yml` and add the new command. Taking `bamboo_missing_location_factor.py` as an example, say we want to be able to run the query for that test by typing in slack `/gitlab datachat run missing_location_factor` then we will need to add to `.gitlab-ci.yml` the following:

```
missing_location_factor:
    stage: chatops
    image: registry.gitlab.com/gitlab-data/data-image/data-image:latest
    only: [chat]
    script:
        - python bamboo_missing_location_factor.py
```

In most cases the only things we need to change are the first line, in this case `missing_location_factor`, that represents how we want to call the command inside slack. And `bamboo_missing_location_factor.py` to be the name of the python file created in the first step.
