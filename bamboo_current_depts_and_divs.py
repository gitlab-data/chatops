import pandas as pd
import os
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine


current_depts_and_divs_str = """
WITH source AS (

  SELECT *
  FROM "ANALYTICS".analytics.employee_directory_analysis

)

SELECT full_name
FROM source
WHERE date_actual = CURRENT_DATE
  AND (department IS NULL OR division IS NULL OR cost_center IS NULL)
  AND CURRENT_DATE > DATEADD('days', 10, hire_date)
"""


def main(query_name: str) -> None:
    """
    Takes a query, runs it in the ANALYTICS database and prints the result
    """
    snowflake_url = URL(
        account="gitlab",
        user=os.environ["SNOWFLAKE_TRANSFORM_USER"],
        password=os.environ["SNOWFLAKE_PASSWORD"],
        database="ANALYTICS",
        warehouse="TRANSFORMING_XS",
        role="TRANSFORMER",
    )
    # authenticator='https://gitlab.okta.com',
    engine = create_engine(snowflake_url)
    connection = engine.connect()
    df1 = pd.read_sql(sql=query_name, con=connection)
    print(df1)


## main
if __name__ == "__main__":
    ## start snowflake engine
    print("starting")
    ## execute query that produces failure result
    main(query_name=current_depts_and_divs_str)
    print("done")
    ## enhance result, e.g. zuora url
